require_relative './../../spec_helper.rb'

describe TranslateModule::TranslateService do
  describe '#call' do
    # success
    it 'Returns something when asked to translate' do
      @translate_service = TranslateModule::TranslateService.new(
        'word' => 'batata',
        'language' => 'en'
      )
      response = @translate_service.call
      expect(response).to match('potato')
    end
    # no word (error)
    it 'Returns error message when no word' do
      @translate_service = TranslateModule::TranslateService.new(
        'language' => 'en'
      )
      response = @translate_service.call
      expect(response).to match('ce nao digitou a palavra!')
    end
  end
end
