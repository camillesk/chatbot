# Service to create list all commands
class HelpService
  def self.call
    response  = "*Meus comandos* \n\n"
    response += "Ajuda\n"
    response += "`Lista de comandos que eu conheço`\n\n"
    response += "Adicione ao Faq\n"
    response += "`Adiciona uma nova questão ao Faq`\n\n"
    response += "Excluir ID\n"
    response += "`Remove uma questão baseada no ID dela`\n\n"
    response += "O que você sabe sobre x\n"
    response += "`Pesquisa por palavra na lista de perguntas e respostas`\n\n"
    response += "Procure hashtag x\n"
    response += "`Lista as perguntas e respostas com aquela hashtag`\n\n"
    response += "Perguntas e Respostas\n"
    response += "`Mostra a lista de perguntas e respostas`\n\n"
    response += "Traduza x\n"
    response += "`Traduz a palavra x para ingles`\n\n"
    response += "Traduza x para y\n"
    response += "`Traduz a palavra x para idioma y`\n\n"
    response
  end
end
