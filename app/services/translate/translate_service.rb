module TranslateModule
  # Service to list faq questions
  class TranslateService
    def initialize(params)
      @word = params['word']
      @language = params['language']
    end

    def call
      if @word.nil?
        response = 'ce nao digitou a palavra!'
      else
        translate_api_key = 'trnsl.1.1.20190718T014509Z.6e6c12cf4d52fc8b.0fdc9fa7a9bc21aa6f9408cac318b0ece23bc656'

        url_get_translation = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=#{translate_api_key}&text=#{@word}&lang=#{@language}"
        res_translation = RestClient.get url_get_translation
        response = JSON.parse(res_translation.body)['text'].to_s
        response.gsub /\"/, ''
      end
    end
  end
end
